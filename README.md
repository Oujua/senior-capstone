In a time where misinformation sells, and news is less about accuracy and more about clicks, it is important to have a way to verify the legitimacy of news in a quick manner. This is where Straight Facts shines. Straight Facts is a website that allows users to submit links to news articles for verification, whether that be automatically scanned and verified by our own automation system or reviewed by an expert. Our team has selected numerous experts covering important topics based on verifiable criteria, such as credentials within their respective fields and education related to certain topics. Straight Facts users have the ability to read more info on why an article's contents are deemed valid or not, view expert credentials, and have the ability to view other user-submitted articles relevant to what they have submitted. 

Overall, the purpose behind the features and user abilities aforementioned is to create a sense of community where individuals help others find truth in uncertain times. Users will have the option to be notified via email of content relevant to their frequent searches as well as trending content to ensure they are always kept informed. Our platform automatically reviews and batches articles based on key words in the articles to ensure the content goes to the most reliable expert, as well as making sure each review is thorough and clear. 



# Link to the project

https://master.d35s8lqekeziyg.amplifyapp.com/


## How to run the app locally

- First, ensure that you have installed Node.js: https://nodejs.org/en/download/.

- Once you have finished this step, type "npm -v". The system should display the npm version installed on your system.

- Clone the repository in Gitlab. 

- Once you have cloned the repository, run the command "npm install -g @aws-amplify/cli" at the root of the project.

- If the previous command runs successfully, run the command "amplify init" and follow the prompt.
(Note: Currently only users with access to the "straight facts" AWS account have the resources to run this step.)

- Navigate to where you cloned the repository in your prefered terminal, and run the "npm start" command below to start the app.  

# Relevant Commands

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm install -g @aws-amplify/cli`

Installs the necessary CLI to properly get amplify files from the repository.

### `amplify init`

Begins the process to enable amplify usage on a local project. 
