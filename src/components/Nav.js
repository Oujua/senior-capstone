import React, { useState } from "react"
import './components.css'
import { NavLink } from "react-router-dom"
import { DropdownButton } from "react-bootstrap";
import DropdownItem from "react-bootstrap/esm/DropdownItem";
import { AmplifySignOut } from '@aws-amplify/ui-react';


const Navbar = () => {
    const [isOpen] = useState(false);
    return (
      <nav
        className="navbar is-primary"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="container">
          <div className="navbar-brand">
            {/*<a
              role="button"
              className={`navbar-burger burger ${isOpen && "is-active"}`}
              aria-label="menu"
              aria-expanded="false"
              onClick={() => setOpen(!isOpen)}
            >
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>*/}
            {/*was producing warning concerning no href attribute, removing does not seem to 
            impact functionality*/}
          </div>
  
          <div className={`navbar-menu ${isOpen && "is-active"}`}>
            <div className="navbar-start">
              <NavLink className="navbar-item" activeClassName="is-active" to="/home">
                Home
              </NavLink>
  
              <NavLink
                className="navbar-item"
                activeClassName="is-active"
                to="/about"
              >
                About
              </NavLink>
  
              <NavLink
                className="navbar-item"
                activeClassName="is-active"
                to="/contact"
              >
                Contact 
              </NavLink>

              <NavLink
                className="navbar-item"
                activeClassName="is-active"
                to="/explore"
              >
                Explore 
              </NavLink>

              <DropdownButton className="profile-button" size="lg" id="dropdown-basic-button" 
                title="User">

                <DropdownItem>
                  <NavLink
                    className="dropdown-navbar"
                    activeClassName="is-active"
                    to="/profile">
                    Profile
                    <AmplifySignOut />
                  </NavLink>
                </DropdownItem>
              </DropdownButton>
            </div>
  
            </div>
        </div>
      </nav>
    );
  };
  
  export default Navbar;