import React, { useState } from 'react'
import './components.css'
import { Link } from 'react-router-dom'
import ExpertSubmit from './ExpertSubmit'
import { SubmitSuccess, SubmitFailure } from './expertSubmission'
import { API } from 'aws-amplify'
import { createCredential as createCredentialMutation } from '../graphql/mutations'

const initState = {
    credId: Math.floor(Math.random() * 1000 + 1),
    ownerID: Math.floor(Math.random() * 1000 + 1),
    name: "",
    university: "",
    degree: "",
    linkedIn: ""
};

const ExpertApp = () => {

    const [formData, setFormData] = useState(initState);
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [isValidSubmission, setIsValidSubmission] = useState();

    async function createSubmission() {
        console.log("createSubmission called");
        
        try {
            await API.graphql({
                query: createCredentialMutation,
                variables: { input: formData },
            });
        } catch (error) {
            console.log(error.errors[0].message);
        }
    }

    function validateSubmission() {
        setIsSubmitted(true);
        if(!formData.name || !formData.university || !formData.degree 
            || !formData.major || !formData.linkedIn) {
            setIsValidSubmission(false);
        } else {
            setIsValidSubmission(true);
        }
    }


    if(isSubmitted && isValidSubmission) {
        return (
            <div>
                <SubmitSuccess />
            </div>
        )
    } else if (isSubmitted && !isValidSubmission) { 
        return (
            <div>
                <SubmitFailure />
            </div>
        )
    } else {
        return (
            <div>
                <Link to={{ pathname: '/profile' }}>
                    Back
                </Link>
               <h1>Expert Application</h1>
    
               <div className='application'>
                    <p className='blurb'>
                        Straight Fact’s mission is to ensure every individual has the ability to 
                        consume news and content that are truthful and factual. In order to do 
                        so, we need you, an expert. Straight Facts utilizes experts with a 
                        certain set of credentials to verify content users submit for verification. 
                        Be sure to apply and Straight Facts will be sure to notify you if you have 
                        been chosen to be a Straight Facts expert. We look forward to reviewing 
                        your application.
                    </p>
    
                    <label for="name"><b>Name</b></label>
                    <input type="text"
                            onChange={(e) =>
                                setFormData({ ...formData, name: e.target.value })
                            }
                            name="name">
                    </input>
    
                    <label for="university"><b>Alma Mater</b></label>
                    <input type="text"
                            onChange={(e) =>
                                setFormData({ ...formData, university: e.target.value })
                            }
                            name="university">
                    </input>
    
                    <label for="degree"><b>Degree Achieved</b></label>
                    <input type="text"
                            onChange={(e) =>
                                setFormData({ ...formData, degree: e.target.value })
                            }
                            name="degree"></input>
    
                    <label for="major"><b>Major/Concentration</b></label>
                    <input type="text"
                            onChange={(e) =>
                                setFormData({ ...formData, major: e.target.value })
                            }
                            name="major"></input>
    
                    <label for="linkedIn"><b>LinkedIn</b></label>
                    <input type="text"
                            onChange={(e) =>
                                setFormData({ ...formData, linkedIn: e.target.value })
                            }
                            name="linkedIn"></input>
    
                    <label for="degreePhoto"><b>Photo of Degree</b></label>
                    <ExpertSubmit />
    
                    <label for="resumePhoto"><b>Resume/CVV</b></label>
                    <ExpertSubmit />
    
                    <br></br>
                    <button 
                        onClick={() => {
                            validateSubmission();
                            createSubmission();
                        }}
                        className="">
                            Submit
                    </button>
                </div>
    
    
            </div>
        )
    }
}

export default ExpertApp;