import React, { useState } from 'react'
import { Storage } from 'aws-amplify';
import './components.css'

const ExpertSubmit = () => {
    const [progress, setProgress] = useState(0);
    const [selectedFile, setSelectedFile] = useState(null);
    const handleFileInput = (e) => {
        setSelectedFile(e.target.files[0]);
    }

    async function uploadFile(file) {
        try {
            await Storage.put(file.name, file);
            setProgress(100);
        } catch (error) {
            console.log(error);
        }

    }

    return <div>
        <div> Upload Progress is {progress}%</div>
        <input type="file" onChange={handleFileInput} />
        <button onClick={() => uploadFile(selectedFile)}> Upload to S3</button>
    </div>
}
export default ExpertSubmit;