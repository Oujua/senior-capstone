import React, { useState } from 'react'
import './components.css'
import { Link } from 'react-router-dom'
import { Auth } from 'aws-amplify'

const Profile = () => {

    const [attr, setAttr] = useState("");

    async function getUsername() {
        const attributes = await Auth.currentAuthenticatedUser();
        let uname = attributes.username;
        setAttr(uname)
        console.log(attr)
    }

    getUsername();

    return (
        <div>
            <h1>Profile</h1>
            <h2>{attr}'s Profile</h2>
            <h2>Recently Submitted Articles</h2>
            <Link to={{ pathname: '/expertApp' }}>
                Apply to be a Straight Facts Expert
            </Link>
        </div>
    )
}

export default Profile;