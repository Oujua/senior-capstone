import React from 'react';
import { Link } from 'react-router-dom'
import './components.css'
import Center from 'react-center-tag'


function DataTable ({data}){

    const columns = data[0] && ["userInput", "factCheck", "category"];

    //headings array for display purposes
    const headings = ["Article", "Status", "Category"]  
    
    return( 
      <Center>
        <table cellPadding={5} cellSpacing={5}> 
           <thead>
             <tr>{data[0] && headings.map((heading)=>  <th className="exploreTableCell"><u>{heading}</u></th>)} </tr>
            </thead>

            <tbody>
              {data.map(row => 
              <tr>{columns.map(columns => <td className="exploreTableCell">{row[columns]}</td>)} <Link to={{
                pathname:'/Results',
                state:row
              }}
              > See Results </Link></tr>)}              
            </tbody>
         </table>
      </Center>
    )      
}

export default DataTable;