import React from 'react';
import './components.css'

function Contact () {
    return (
        <div className="contact">
            <h1>Contact Us</h1>
            <p>Questions? Concerns? Suggestions?</p>
            <label htmlFor="email">Email us: </label>
            <a href="mailto:straightfacts@gmail.com" name="email">straightfacts@gmail.com</a>
        </div>
    );
}

export default Contact;