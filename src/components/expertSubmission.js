import React from 'react';
import './components.css'
import { Link } from 'react-router-dom'

export const SubmitSuccess = () => {
    return (
        <div className="submission">
            <h1>Success!</h1>
            <p>Thank you for submitting your application!</p>
            <p>You will be notified if you are selected to be a Straight Facts Expert.</p>
            <Link to={{ pathname: '/profile' }}>
                Back to Profile
            </Link>
        </div>
    )
}

export const SubmitFailure = () => {
    return (
        <div className="submission">
            <h1>Denied</h1>
            <p>Your application has not been submitted due to missing fields.</p>
            <p>Please ensure all fields are filled prior to submission.</p>
            <Link to={{ pathname: '/profile' }}>
                Back to Profile
            </Link>
        </div>
    )
}

