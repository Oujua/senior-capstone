import React, { useState, useEffect } from 'react';
import './components.css'
import * as queries from "../graphql/queries"
import { API } from "aws-amplify";
import DataTable from './DataTable'

function Explore() {

  const [q, setQ] = useState("");

  const [fetchedPosts, setFetchedPosts] = useState([]);

  useEffect(() => {
    async function listPosts() {
      try {
        const allPosts = await API.graphql({
          query: queries.listPosts
        });

        setFetchedPosts(allPosts.data.listPosts.items)
      } catch (error) {
        console.log(error);
      }
    }
    listPosts();

  })
  function search(rows) {

    return rows.filter(
      (row) =>
        row.userInput.toLowerCase().indexOf(q) > -1 ||
        row.factCheck.toLowerCase().indexOf(q) > -1 ||
        row.category.toLowerCase().indexOf(q) > -1

    );

  }

  return (
    <div>
      <h1>Explore Submissions</h1>
      <h2>
        <label htmlFor="topics">Sort: </label>
        <select
          name="topics"
          id="topics"
          value={q}
          onChange={(e) => setQ(e.target.value)}
        >
          <option value="">All</option>
          <option value="entertainment">Entertainment</option>
          <option value="politics">Politics</option>
          <option value="conspiracy">Conspiracy</option>
        </select>
      </h2>
      <div>
        <DataTable data={search(fetchedPosts)} />
      </div>
    </div>
  )


}


export default Explore;
