import React from 'react';
import './components.css'
import checkmark from '../assets/checkmark.png'
import failure from '../assets/failure.png'

export const Success = () => {
    return (
        <div className="submission">
            <h1>Recieved</h1>
            <img src={checkmark} alt=""/>
            <p>Your link is set to be reviewed by our team of experts.</p>
            <p>You will recieve a link to your results in your email.</p>
            <a href="./home">Submit another link</a>
        </div>
    )
}

export const Failure = () => {
    return (
        <div className="submission">
            <h1>Denied</h1>
            <img src={failure} alt=""/>
            <p>The link you submitted has not been verified by our service as a reputable news source.</p>
            <p>It may be from a satire/spoof site, or is formatted incorrectly.</p>
            <a href="./home">Please try again.</a>
        </div>
    )
}

