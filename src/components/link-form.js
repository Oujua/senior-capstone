import React, { useState } from 'react'
import { API } from 'aws-amplify'
import { createPost as createPostMutation } from "../graphql/mutations";
import { Success, Failure } from "../components/Submission";

const initState = {
    postId: Math.floor(Math.random() * 1000 + 1),
    userInput: "",
    factCheck: "Verifying",
    category: "Entertainment",
    explanation: "This is an explanation",
    flagged: false,
    posterId: Math.floor(Math.random() * 1000 + 1),
    expertId: Math.floor(Math.random() * 1000 + 1),
};

const LinkForm = () => {
    const [posts, setPosts] = useState([]);
    const [formData, setFormData] = useState(initState);
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [isValidLink, setIsValidLink] = useState();

    async function createPost() {
        console.log("createPost called");
        if (!formData.userInput) return;

        try {
            await API.graphql({
                query: createPostMutation,
                variables: { input: formData },
            });
            setPosts([...posts, formData]);
            setFormData(initState);
        } catch (error) {
            console.log(error.errors[0].message);
        }
    }

    function validateLink() {
        setIsSubmitted(true);
        if (!formData.userInput) {
            setIsValidLink(false);
        } else {
            setIsValidLink(true);
        }
    }

    if (isSubmitted && isValidLink) {
        return (
            <div>
                <Success />
            </div>
        );
    } else if (isSubmitted && !isValidLink) {
        return (
            <div>
                <Failure />
            </div>
        );
    } else {
        return (
            <div className="link-form">
                <input
                    type="text"
                    onChange={(e) =>
                        setFormData({ ...formData, userInput: e.target.value })
                    }
                    placeholder="Enter URL here..."
                    value={formData.userInput}
                ></input>
                <br></br>
                <label htmlFor="topics">Category: </label>
                <select
                    name="topics"
                    id="topics"
                    required
                    onChange={(e) => {
                        setFormData({ ...formData, category: e.target.value });
                    }}
                >
                    <option value="Entertainment">Entertainment</option>
                    <option value="Politics">Politics</option>
                    <option value="Conspiracy">Conspiracy</option>
                </select>
                <br></br>

                <button
                    onClick={() => {
                        createPost();
                        validateLink();
                    }}
                >
                    Submit
                </button>
            </div>
        );
    }
};

export default LinkForm;