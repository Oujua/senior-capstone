import React from 'react';
import LinkForm from '../components/link-form';

function Home () {
    return (
        <div className="header">
            <h1>Straight Facts</h1>
            <LinkForm />
        </div>
    );
}

export default Home;