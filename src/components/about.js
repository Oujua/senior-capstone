import React from 'react';
import './components.css';

function About () {
    return (
        <div className="about">
            <h1>About Us</h1>
            <p>
                In a time where misinformation sells, and news is less about accuracy
                and more about clicks, it is important to have a way to verify the
                legitimacy of news in a quick manner. This is where <i><b>Straight Facts</b></i> shines. 
                Our goal is to allow users to submit the news they read everyday, and allow 
                our service to verify such news as based in fact, or based in fiction.
            </p>
            <p>
                Our verification process relies on two services: automated review, and review by 
                verified experts in their respective field of study. Numerous services, such as the 
                ability to vote/comment on reviewed articles, both help us with vital feedback on our 
                end, and help create a community of well-informed and factually literate users. Feel free
                to contact us using the email on our Contact page.
            </p>
            <b>- Straight Facts Team</b>
        </div>
    );
}

export default About;