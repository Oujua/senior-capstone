import React from 'react'
import { AmplifySignOut } from '@aws-amplify/ui-react';
import './components.css'

const SignOut = () => {
    return (
        <div className="signout">
            <AmplifySignOut />
        </div>
    )
}

export default SignOut;