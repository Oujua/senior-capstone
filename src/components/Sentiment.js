import { Predictions } from 'aws-amplify';
import { Article } from 'newspaperjs';
import { useState, useEffect } from 'react';

const proxyString = "http://api.scraperapi.com?api_key=a22c258ea4fe6a95d85533dacf81e768&url=";

export default function Sentiment(props) {

    const [sentimentValues, setSentimentValues] = useState({});
    const [foundSentiment, setFoundSentiment] = useState(false);

    async function getSentiment(link) {

        Article(proxyString + link)
            .then(result => {
                return result.text.slice(0, 4000)
            }).then(result => {
                //console.log(result)
                return Predictions.interpret({
                    text: {
                        source: {
                            text: result,
                        },
                        type: "ALL"
                    }
                })
            }).then(data => {
                //console.log(data)
                setSentimentValues(data.textInterpretation.sentiment);
                setFoundSentiment(true);
            }).catch(error => {
                console.log(error)
                setFoundSentiment(false);
            })
    }

    useEffect(() => {
        getSentiment(props.link);
    }, [props.link])
    if (foundSentiment) {
        return (
            <div>
                <h1>Article Sentiment</h1>
                <h3>{sentimentValues.predominant}</h3>
                <p>Positive: {sentimentValues.positive}</p>
                <p>Neutral: {sentimentValues.neutral}</p>
                <p>Negative: {sentimentValues.negative}</p>
            </div>
        )
    }
    else {
        return (
            <div>
                <h1>Article Sentiment</h1>
                <h3>Unable to determine sentiment</h3>
            </div>
        )
    }

}
