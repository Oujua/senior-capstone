import React from 'react';
import { DiscussionEmbed } from 'disqus-react';
import Sentiment from './Sentiment';


function Results(data) {
    let temp = data.location.state;
    console.log(temp);
    const disqusShortName = "straightfacts"
    const disqusConfig = {
        url: "http://localhost:3000",
        identifier: temp.postId,
        title: temp.userInput
    }

    return (
        <div>
            <h1>Results</h1>
            <p>Post ID: {temp.postId}</p>
            <p>{temp.category}</p>
            <div className="results">
                <p>We examined the article:</p>
                <a href={temp.userInput}>{temp.userInput}</a>
                <p>and found that it is {temp.factCheck}.</p>
                <p>{temp.explanation}</p>
            </div>
            <Sentiment link={temp.userInput} />
            <DiscussionEmbed
                shortname={disqusShortName}
                config={disqusConfig}
            />
        </div>
    )

}


export default Results;