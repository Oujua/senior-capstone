import React from "react";
import Navbar from './components/Nav';
import About from './components/about';
import Contact from './components/contact';
import Explore from './components/explore';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import ExpertApp from './components/expertApp';
import "./App.css";
import "./components/components.css"
import { withAuthenticator } from "@aws-amplify/ui-react";
import Home from "./components/home";
import Results from "./components/Results"
import Profile from "./components/profile"

function App() {
  return (
    <div className="App">
      {/*<SignOut /> SignOut component made redundant by profile dropdown
                     button (remove?)*/}
      <Router>
        <Navbar />
        <div className="container mt-2" style={{ marginTop: 40 }}>
          <Switch>
            <Route exact path="/">
              <Redirect to="/home" />
            </Route>
            <Route exact path="/about" component={ About } />
            <Route exact path="/contact" component={ Contact } />
            <Route exact path="/explore" component={ Explore } />
            <Route exact path="/home" component={ Home } />
            <Route exact path="/Results" component={ Results } />
            <Route exact path="/profile" component={ Profile } />
            <Route exact path="/expertApp" component={ ExpertApp } />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default withAuthenticator(App);
