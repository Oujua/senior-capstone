/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreatePost = /* GraphQL */ `
  subscription OnCreatePost {
    onCreatePost {
      id
      postId
      userInput
      factCheck
      category
      explanation
      flagged
      posterId
      expertId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePost = /* GraphQL */ `
  subscription OnUpdatePost {
    onUpdatePost {
      id
      postId
      userInput
      factCheck
      category
      explanation
      flagged
      posterId
      expertId
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePost = /* GraphQL */ `
  subscription OnDeletePost {
    onDeletePost {
      id
      postId
      userInput
      factCheck
      category
      explanation
      flagged
      posterId
      expertId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser {
    onCreateUser {
      id
      userID
      email
      isExpert
      credentials {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      userName
      posts
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser {
    onUpdateUser {
      id
      userID
      email
      isExpert
      credentials {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      userName
      posts
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser {
    onDeleteUser {
      id
      userID
      email
      isExpert
      credentials {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      userName
      posts
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCredential = /* GraphQL */ `
  subscription OnCreateCredential {
    onCreateCredential {
      id
      credId
      ownerID
      name
      file
      linkedIn
      degree
      major
      university
      category
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCredential = /* GraphQL */ `
  subscription OnUpdateCredential {
    onUpdateCredential {
      id
      credId
      ownerID
      name
      file
      linkedIn
      degree
      major
      university
      category
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCredential = /* GraphQL */ `
  subscription OnDeleteCredential {
    onDeleteCredential {
      id
      credId
      ownerID
      name
      file
      linkedIn
      degree
      major
      university
      category
      createdAt
      updatedAt
    }
  }
`;
