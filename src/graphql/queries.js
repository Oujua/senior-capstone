/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getPost = /* GraphQL */ `
  query GetPost($id: ID!) {
    getPost(id: $id) {
      id
      postId
      userInput
      factCheck
      category
      explanation
      flagged
      posterId
      expertId
      createdAt
      updatedAt
    }
  }
`;
export const listPosts = /* GraphQL */ `
  query ListPosts(
    $filter: ModelPostFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        postId
        userInput
        factCheck
        category
        explanation
        flagged
        posterId
        expertId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      userID
      email
      isExpert
      credentials {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      userName
      posts
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userID
        email
        isExpert
        credentials {
          id
          credId
          ownerID
          name
          file
          linkedIn
          degree
          major
          university
          category
          createdAt
          updatedAt
        }
        userName
        posts
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCredential = /* GraphQL */ `
  query GetCredential($id: ID!) {
    getCredential(id: $id) {
      id
      credId
      ownerID
      name
      file
      linkedIn
      degree
      major
      university
      category
      createdAt
      updatedAt
    }
  }
`;
export const listCredentials = /* GraphQL */ `
  query ListCredentials(
    $filter: ModelCredentialFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCredentials(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
