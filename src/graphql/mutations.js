/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createPost = /* GraphQL */ `
  mutation CreatePost(
    $input: CreatePostInput!
    $condition: ModelPostConditionInput
  ) {
    createPost(input: $input, condition: $condition) {
      id
      postId
      userInput
      factCheck
      category
      explanation
      flagged
      posterId
      expertId
      createdAt
      updatedAt
    }
  }
`;
export const updatePost = /* GraphQL */ `
  mutation UpdatePost(
    $input: UpdatePostInput!
    $condition: ModelPostConditionInput
  ) {
    updatePost(input: $input, condition: $condition) {
      id
      postId
      userInput
      factCheck
      category
      explanation
      flagged
      posterId
      expertId
      createdAt
      updatedAt
    }
  }
`;
export const deletePost = /* GraphQL */ `
  mutation DeletePost(
    $input: DeletePostInput!
    $condition: ModelPostConditionInput
  ) {
    deletePost(input: $input, condition: $condition) {
      id
      postId
      userInput
      factCheck
      category
      explanation
      flagged
      posterId
      expertId
      createdAt
      updatedAt
    }
  }
`;
export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      userID
      email
      isExpert
      credentials {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      userName
      posts
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      userID
      email
      isExpert
      credentials {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      userName
      posts
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      userID
      email
      isExpert
      credentials {
        id
        credId
        ownerID
        name
        file
        linkedIn
        degree
        major
        university
        category
        createdAt
        updatedAt
      }
      userName
      posts
      createdAt
      updatedAt
    }
  }
`;
export const createCredential = /* GraphQL */ `
  mutation CreateCredential(
    $input: CreateCredentialInput!
    $condition: ModelCredentialConditionInput
  ) {
    createCredential(input: $input, condition: $condition) {
      id
      credId
      ownerID
      name
      file
      linkedIn
      degree
      major
      university
      category
      createdAt
      updatedAt
    }
  }
`;
export const updateCredential = /* GraphQL */ `
  mutation UpdateCredential(
    $input: UpdateCredentialInput!
    $condition: ModelCredentialConditionInput
  ) {
    updateCredential(input: $input, condition: $condition) {
      id
      credId
      ownerID
      name
      file
      linkedIn
      degree
      major
      university
      category
      createdAt
      updatedAt
    }
  }
`;
export const deleteCredential = /* GraphQL */ `
  mutation DeleteCredential(
    $input: DeleteCredentialInput!
    $condition: ModelCredentialConditionInput
  ) {
    deleteCredential(input: $input, condition: $condition) {
      id
      credId
      ownerID
      name
      file
      linkedIn
      degree
      major
      university
      category
      createdAt
      updatedAt
    }
  }
`;
